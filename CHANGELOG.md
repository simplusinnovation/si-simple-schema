# Changelog
All notable changes to this library will be documented in this file.

This project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

[comment]: <> (######################################################)
[comment]: <> (############ Version ?.?.? Release ###################)
[comment]: <> (######################################################)
## [Unreleased]

[comment]: <> (######################################################)
[comment]: <> (############ Version ?.?.? Release ###################)
[comment]: <> (######################################################)
## [0.2.0] - 2018-04-12
### CHANGES - BREAKING
- `validate` methos is no longer asynchronous. We no longer return a promise on this method.

    This will no longer be the case :
    ```SimpleSchema(obj).validate.then(value=>{}).catch({}) => value : {status = true, content: obj, error: null}```

    Instead use `validate` as follows:
    ```const value = SimpleSchema(obj).validate => value : {status = true, content: obj, error: null}```

### NEW
#### Rules
- Array `dataType` added.
- New rules to ModelSchema:
  * `contains:` used only in array to validate the input type an array can take. if not specified, array can take any input.
  * `hidden:` used in the `sanitize` method to remove unwanted output.
#### Methods
- `sanitize` method that strips object of unwanted output.
### FIXES
#### General
- Tslint.json to general standard used in all projects.
#### Methods
- `validate:` method can now take an array of objects to validate.


[comment]: <> (######################################################)
[comment]: <> (############ Version 0.1.1 Release ###################)
[comment]: <> (######################################################)
## [0.1.1] - 2018-03-21
[comment]: <> (### CHANGES - BREAKING)
### NEW
#### General
- Changelog
- TSLint
- TSConfig
- Pipelines
#### Rules
- Added following validation rules:
  * `dataType:` : data type validation for String, Number, Date, Boolean.
  * `defaultValue:` adds a default value to your object if it not defined.
  * `name:` creates and alias for the column name to use on errors and other messages.
  * `required:` makes the field required in all objects.
  * `trim:` trims leading and algging whitespaces to string inputs.
  * `allowed:` creates a list of allowed inputs for key.
- Done tests for rules.
#### Methods
- `validate` method that applies rules to input and validates input.
- `define` method that parses schema into a table definition in the format below:
  * `sql:` parse into sql definition consumable by `npm-sql`.
- Done tests for rules.
[comment]: <> (### FIXES)