import { Modelschema, SimpleSchema } from '../../build';
import chai = require('chai');
import { omit } from 'lodash';

describe('Examples - 1', function(): void {
	describe('Test for various example cases', function(): void {
		const validator = new SimpleSchema({
			account: {dataType: Number, notNull: true},
			profile: {
				name : {dataType: String},
				surname: {dataType: String}
			}
		} as Modelschema)
		const inputs = []
		it('should pass nested schema object', function(done: MochaDone): void {
			const value = validator.validate({account: 1234567, profile: {name: 'Phillip', surname: 'Morrison'}})
			chai.expect(value.status).to.be.true
			chai.expect(value).to.be.an('object')
			chai.expect(value.content[0]).to.deep.equal({account: 1234567, profile: {name: 'Phillip', surname: 'Morrison'}})
			done()
			// .then((value) => {
			// })
			// .catch(done)
		})
		it('should fail nested schema object', function(done: MochaDone): void {
			const value = validator.validate({account: 1234567, profile: {name: 1, surname: 'Morrison'}})
			chai.expect(value.status).to.be.false
			chai.expect(value).to.be.an('object')
			done()
			// .then((value) => {
			// })
			// .catch(done)
		})
	})
})