import { Modelschema, SimpleSchema } from '../../build';
import chai = require('chai');
import { omit } from 'lodash';

describe('Examples - 1', function(): void {
	describe('Test for various example cases', function(): void {
		const validator = new SimpleSchema({
			name: {dataType: String},
			surname: {dataType: String},
			email: {dataType: String, notNull: true, unique: true},
			age: {dataType: Number},
			gender: {dataType: String, allowed: ['Male', 'Female']},
			createdAt: {dataType: Date, defaultValue: new Date()},
			permissons: {dataType: Array, contains: String}
		})
		const inputs = []
		it('should pass single object input validation', function(done: MochaDone): void {
			inputs.push({name: 'Athenkosi', surname: 'Mase', email: 'Athenkosi@domain.co.za', age: 12, gender : 'Male', permissons: ['admin']})
			const value = validator.validate({name: 'Athenkosi', surname: 'Mase', email: 'Athenkosi@domain.co.za', age: 12, gender : 'Male', permissons: ['admin']})
			chai.expect(value.status).to.be.true
			chai.expect(value).to.be.an('object')
			chai.expect(omit(value.content[0], 'createdAt')).to.deep.equal({name: 'Athenkosi', surname: 'Mase', email: 'Athenkosi@domain.co.za', age: 12, gender : 'Male', permissons: ['admin']})
			done()
			// .then((value) => {
			// })
			// .catch(done)
		})
		it('should pass multiple object array input validation', function(done: MochaDone): void {
			inputs.push({name: 'Yehudi', surname: 'Holleveot', email: 'Yehudi@domain.co.za', age: 18, gender : 'Male', permissons: ['admin']})
			const value = validator.validate(...inputs)
			chai.expect(value.status).to.be.true
			chai.expect(value).to.be.an('object')
			done()
			// .then((value) => {
			// })
			// .catch(done)
		})
		it('should fail uniquness validation', function(done: MochaDone): void {
			inputs.push({name: 'Faisal', surname: 'Habib', email: 'Athenkosi@domain.co.za', age: 21, gender : 'Male', permissons: ['admin']})
			const value = validator.validate(...inputs)
			chai.expect(value.status).to.be.false
			chai.expect(value).to.be.an('object')
			done()
			// .then((value) => {
			// })
			// .catch(done)
		})
	})
})