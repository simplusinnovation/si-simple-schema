import { Modelschema, SimpleSchema } from '../../build';
import chai = require('chai');
import { omit } from 'lodash';

describe('Examples - 3', function(): void {
	describe('Test for various example cases', function(): void {
		const validator = new SimpleSchema({
			_key: {dataType: String,  notNull: true, hidden: true},
			img: {dataType: String},
			name: {dataType: String}
		} as Modelschema)
		const inputs = []
		it('should sanitize hidden key object', function(done: MochaDone): void {
			const value = validator.sanitize({_key: '123456', img: 'https://img.com', name: 'Paul'})
			chai.expect(value.status).to.be.true
			chai.expect(value).to.be.an('object')
			chai.expect(value.content[0]).to.deep.equal({img: 'https://img.com', name: 'Paul'})
			done()
			// .then((value) => {
			// })
			// .catch(done)
		})
	})
})