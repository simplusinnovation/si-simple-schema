import { Modelschema, SimpleSchema } from '../../build'
import chai = require('chai')

interface TestCases {
	[x: string]: {field: string| number | boolean | Date}
}

describe('Default value Tests', function(): void {
	describe('Test for default value', function(): void {
		it('should insert the default value item on validation ', function(done: MochaDone): void {
			const validator = new SimpleSchema({field: {dataType: String}, field2: {dataType: String, defaultValue: 'default'}})
			const value = validator.validate({field : 'Male'})
			const f = 'field2'
			chai.expect(value.status).to.be.true
			chai.expect(value).to.be.an('object')
			chai.expect(value.content[0][f]).to.equal('default')
			done()
			// .then((value) => {
			// })
			// .catch(done)
		})
	})
})