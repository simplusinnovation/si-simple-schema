import { Modelschema, SimpleSchema } from '../../build';
import { ValidationError } from 'joi';
import chai = require('chai')

describe('Required input Tests', function(): void {
	describe('Test required field assertion', function(): void {
		it('should fail the validation because the required field is missing', function(done: MochaDone): void {
			const validator = new SimpleSchema({field: {dataType: String}, req: {dataType: Boolean , notNull: true}})
			const value = validator.validate({field: 'This field is not required'})
			const target = (value.error as ValidationError).details[0]
			chai.expect(value.status).to.be.false
			chai.expect(value).to.be.an('object')
			// chai.expect(target.message).to.equal('"req" is required')
			done()
			// .then((value) => {
			// })
			// .catch(done)
		})
		it('should pass the validation because the required field is present', function(done: MochaDone): void {
			const validator = new SimpleSchema({field: {dataType: String, notNull: false}, req: {dataType: Boolean , notNull: true}})
			const value = validator.validate({req: true})
			chai.expect(value.status).to.be.true
			chai.expect(value).to.be.an('object')
			done()
			// .then((value) => {
			// })
			// .catch(done)
		})
	})
})