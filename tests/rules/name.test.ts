import { Modelschema, SimpleSchema } from '../../build'
import { ValidationError } from 'joi';
import chai = require('chai')


describe('Field Name Tests', function(): void {
	describe('Test for field label in error messages', function(): void {
		it('should return an error with alias name used instead of field name ', function(done: MochaDone): void {
			const validator = new SimpleSchema({field: {dataType: String, name: 'Alias'}})
			const value = validator.validate({field: 1})
			const target = (value.error as ValidationError).details[0]
			chai.expect(value.status).to.be.false
			chai.expect(value).to.be.an('object')
			chai.expect(target.message).to.equal('"Alias" must be a string')
			chai.expect(target.context.label).to.equal('Alias')
			done()
			// .then((value) => {
			// })
			// .catch(done)
		})
	})
})