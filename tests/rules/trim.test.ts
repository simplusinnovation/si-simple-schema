import { Modelschema, SimpleSchema } from '../../build'
import chai = require('chai')

describe('Trim input Tests', function(): void {
	describe('Test for trim function', function(): void {
		it('should trim the leading and lagging tabs on validation ', function(done: MochaDone): void {
			const validator = new SimpleSchema({field: {dataType: String, trim: true}})
			const value = validator.validate({field: '	String to leading and lagging tabs	'})
			const f = 'field'
			chai.expect(value.status).to.be.true
			chai.expect(value).to.be.an('object')
			chai.expect(value.content[0][f]).to.equal('String to leading and lagging tabs')
			done()
			// .then((value) => {
			// })
			// .catch(done)
		})
		it('should trim the leading and lagging spaces on validation', function(done: MochaDone): void {
			const validator = new SimpleSchema({field: {dataType: String, trim: true}})
			const value = validator.validate({field: '     String with leading and lagging spaces    '})
			const f = 'field'
			chai.expect(value.status).to.be.true
			chai.expect(value).to.be.an('object')
			chai.expect(value.content[0][f]).to.equal('String with leading and lagging spaces')
			done()
			// .then((value) => {
			// })
			// .catch(done)
		})
	})
})