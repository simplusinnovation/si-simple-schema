import { Modelschema, SimpleSchema } from '../../build';
import chai = require('chai')

const validator = new SimpleSchema({field: {dataType: String, allowed: ['Male', 'Female']}} as Modelschema)

describe('Allowed input Tests', function(): void {
	describe('Test for allowed enum', function(): void {
		it('should pass the allowed validation ', function(done: MochaDone): void {
			const value = validator.validate({field : 'Male'})
			chai.expect(value.status).to.be.true
			chai.expect(value).to.be.an('object')
			chai.expect(value.content[0]).to.deep.equal({field : 'Male'})
			done()
			// .then((value) => {
			// })
			// .catch(done)
		})
		it('should fail the allowed validation ', function(done: MochaDone): void {
			const value = validator.validate({field : 'Mike'})
			chai.expect(value.status).to.be.false
			chai.expect(value).to.be.an('object')
			done()
			// .then((value) => {
			// })
			// .catch(done)
		})
	})
})