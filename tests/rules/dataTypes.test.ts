import { Modelschema, SimpleSchema } from '../../build'
import chai = require('chai')

interface TestCases {
	// tslint:disable-next-line:no-any
	[x: string]: {field: string| number | boolean | Date | any[]}
}

describe('Data Types Tests', function(): void {
	// tslint:disable-next-line:no-shadowed-variable
	const TestCases: TestCases = {
		string: {field : 'StringInput'},
		number: {field : 1234},
		boolean: {field: true},
		date: {field: new Date()},
		array: {field: ['item1', 'item2']}
	}
	describe('Test for accurate data type validation', function(): void {
		it('should pass the string validation ', function(done: MochaDone): void {
			const validator = new SimpleSchema({field: {dataType: String}} as Modelschema)
			const value = validator.validate(TestCases.string)
			chai.expect(value.status).to.be.true
			chai.expect(value).to.be.an('object')
			chai.expect(value.content[0]).to.deep.equal(TestCases.string)
			done()
			// .then((value) => {
			// })
			// .catch(done)
		})
		it('should pass the number validation ', function(done: MochaDone): void {
			const validator = new SimpleSchema({field: {dataType: Number}} as Modelschema)
			const value = validator.validate(TestCases.number)
			chai.expect(value.status).to.be.true
			chai.expect(value).to.be.an('object')
			chai.expect(value.content[0]).to.deep.equal(TestCases.number)
			done()
			// .then((value) => {
			// })
			// .catch(done)
		})
		it('should pass the date validation ', function(done: MochaDone): void {
			const validator = new SimpleSchema({field: {dataType: Date}} as Modelschema)
			const value = validator.validate(TestCases.date)
			chai.expect(value.status).to.be.true
			chai.expect(value).to.be.an('object')
			chai.expect(value.content[0]).to.deep.equal(TestCases.date)
			done()
			// .then((value) => {
			// })
			// .catch(done)
		})
		it('should pass the boolean validation ', function(done: MochaDone): void {
			const validator = new SimpleSchema({field: {dataType: Boolean}} as Modelschema)
			const value = validator.validate(TestCases.boolean)
			chai.expect(value.status).to.be.true
			chai.expect(value).to.be.an('object')
			chai.expect(value.content[0]).to.deep.equal(TestCases.boolean)
			done()
			// .then((value) => {
			// })
			// .catch(done)
		})
		it('should pass the array validation ', function(done: MochaDone): void {
			const validator = new SimpleSchema({field: {dataType: Array}} as Modelschema)
			const value = validator.validate(TestCases.array)
			chai.expect(value.status).to.be.true
			chai.expect(value).to.be.an('object')
			chai.expect(value.content[0]).to.deep.equal(TestCases.array)
			done()
			// .then((value) => {
			// })
			// .catch(done)
		})
	})
})