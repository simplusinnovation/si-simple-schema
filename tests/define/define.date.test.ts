import { Dialects, Definition } from '../../src/ModelSchema';
import { Modelschema, SimpleSchema } from '../../build';
import { forOwn } from 'lodash';
import chai = require('chai')


// ['mssql' , 'oracle' , 'mysql'  , 'postgres' , 'sqlite']

describe('Define table schema Tests', function(): void {
	describe('Test for sql table definition - date', function(): void {
		it('should return a sql column definition for dates', function(done: MochaDone): void {
			const table = new SimpleSchema({
				dateField: {dataType: Date}
			} as Modelschema)
			const technologies = ['mssql', 'mysql', 'postgres', 'sqlite'] as Dialects[]
			technologies.forEach((tech) => {
				const t = forOwn(table.define('sql', tech) as {[x: string]: Definition<string, {dataType: string}>}, (value, key) => {
					// Date
					switch (tech) {
						case 'mssql' || 'mysql' || 'sqlite':
							chai.expect(value.dataType).to.equal('DATETIME')
							break;
						case 'oracle' || 'postgres':
							chai.expect(value.dataType).to.equal('TIMESTAMP')
							break;
					}
				})
			})
			done()
		})
	})
})