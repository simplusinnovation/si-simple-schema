import { Dialects, Definition } from '../../src/ModelSchema';
import { Modelschema, SimpleSchema } from '../../build';
import { forOwn } from 'lodash';
import chai = require('chai')

// ['mssql' , 'oracle' , 'mysql'  , 'postgres' , 'sqlite']
describe('Define table schema Tests', function(): void {
	describe('Test for sql table definition - number', function(): void {
		it('should return a sql column definition for number', function(done: MochaDone): void {
			const table = new SimpleSchema({
				numberFeild: {dataType: Number}
			} as Modelschema)
			const technologies = ['sqlite'] as Dialects[]
			technologies.forEach((tech) => {
				const t = forOwn(table.define('sql', tech) as {[x: string]: Definition<string, {dataType: string}>}, (value, key) => {
					// Number
					switch (tech) {
						case 'mssql':
							chai.expect(value.dataType).to.equal('NUMERIC(20 , 2)')
							break;
						case 'oracle':
							chai.expect(value.dataType).to.equal('NUMBER(* , 2)')
							break;
						case 'mysql':
							chai.expect(value.dataType).to.equal('DOUBLE(20 , 2)')
							break;
						case 'postgres':
							chai.expect(value.dataType).to.equal('FLOAT(2)')
							break;
						case 'sqlite':
							chai.expect(value.dataType).to.equal('DECIMAL(20, 2)')
							break;
					}
				})
			})
			done()
		})
	})
})