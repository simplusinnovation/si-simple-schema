import { Dialects, Definition } from '../../src/ModelSchema';
import { Modelschema, SimpleSchema } from '../../build';
import { forOwn } from 'lodash';
import chai = require('chai');

interface TestCases {
	[x: string]: {field: string| number | boolean | Date}
}

describe('Define table schema Tests', function(): void {
	describe('Test for sql table definition - boolean', function(): void {
		it('should return a sql column definition for booleans', function(done: MochaDone): void {
			const table = new SimpleSchema({
				booleanField: {dataType: Boolean},
			} as Modelschema)
			const technologies = ['mssql' , 'oracle' , 'mysql'  , 'postgres' , 'sqlite'] as Dialects[]
			technologies.forEach((tech) => {
				const t = forOwn(table.define('sql', tech) as {[x: string]: Definition<string, {dataType: string}>}, (value, key) => {
					// Boolean
					chai.expect(value.dataType).to.equal('VARCHAR(5)')
				})
			})
			done()
		})
	})
})