import { Dialects, Definition } from '../../src/ModelSchema';
import { Modelschema, SimpleSchema } from '../../build';
import { forOwn } from 'lodash';
import chai = require('chai')

// ['mssql' , 'oracle' , 'mysql'  , 'postgres' , 'sqlite']
describe('Define table schema Tests', function(): void {
	describe('Test for sql table definition - integer', function(): void {
		it('should return a sql column definition for integer', function(done: MochaDone): void {
			const table = new SimpleSchema({
				integerField: {dataType: 'Integer'},
			} as Modelschema)
			const technologies = ['mssql', 'postgres' , 'sqlite'] as Dialects[]
			technologies.forEach((tech) => {
				const t = forOwn(table.define('sql', tech) as {[x: string]: Definition<string, {dataType: string}>}, (value, key) => {
					// Integer
					switch (tech) {
						case 'mssql' || 'postgres' || 'sqlite':
							chai.expect(value.dataType).to.equal('BIGINT')
							break;
						case 'oracle':
							chai.expect(value.dataType).to.equal('NUMBER(* , 0)')
							break;
						case 'mysql':
							chai.expect(value.dataType).to.equal('BIGINT(19)')
							break;
					}
				})
			})
			done()
		})
	})
})