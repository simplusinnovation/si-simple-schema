import { Dialects, Definition } from '../../src/ModelSchema';
import { Modelschema, SimpleSchema } from '../../build';
import { forOwn } from 'lodash';
import chai = require('chai')

interface TestCases {
	[x: string]: {field: string| number | boolean | Date}
}

describe('Define table schema Tests', function(): void {
	describe('Test for sql table definition - string', function(): void {
		it('should return a sql column definition for strings', function(done: MochaDone): void {
			const table = new SimpleSchema({
				stringField : {dataType: String},
				stringField22: {dataType: String, max: 22},
			} as Modelschema)
			const technologies = ['mssql' , 'oracle' , 'mysql'  , 'postgres' , 'sqlite'] as Dialects[]
			technologies.forEach((tech) => {
				const t = forOwn(table.define('sql', tech) as {[x: string]: Definition<string, {dataType: string}>}, (value, key) => {
					// String
					switch (key) {
						case 'stringField':
							chai.expect(value.dataType).to.equal('VARCHAR(255)')
							break;
						case 'stringField22':
							chai.expect(value.dataType).to.equal('VARCHAR(22)')
							break;
					}
				})
			})
			done()
		})
	})
})