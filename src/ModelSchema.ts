export type Types = StringConstructor | 'String' | NumberConstructor | 'Number' | DateConstructor  | 'Date' | BooleanConstructor | 'Boolean' | 'Integer' | ArrayConstructor | 'Array' | string
export type mapper = 'joi' | 'sql';
export type Technology = 'sql'

export interface SchemaItems<Name extends string, Type> {
	dataType: Types;
	primaryKey?: boolean;
	references?: {
		table: string;
		column: string;
		onDelete?: 'restrict' | 'cascade' | 'no action' | 'set null' | 'set default';
		onUpdate?: 'restrict' | 'cascade' | 'no action' | 'set null' | 'set default';
	};
	notNull?: boolean;
	unique?: boolean;
	defaultValue?: string | number | Date | boolean | Type
	// Rule not applied to standard SQL
	trim?: boolean;
	allowed?: string[] | number[] | boolean[];
	name?: Name | undefined
	max?: number
	min?: number
	// Special cases only applied to arrays
	contains?: Types
	hidden?: Boolean
}
export interface Named<Name extends string> {
	name?: Name;
}
export interface Definition<Name extends string, Type> extends Named<Name> {
	jsType?: Type
	dataType: string;
	primaryKey?: boolean;
	references?: {
		table: string;
		column: string;
		onDelete?: 'restrict' | 'cascade' | 'no action' | 'set null' | 'set default';
		onUpdate?: 'restrict' | 'cascade' | 'no action' | 'set null' | 'set default';
	};
	notNull?: boolean;
	unique?: boolean;
	defaultValue?: Type;
}

export interface Modelschema {
	[x: string]: SchemaItems<string, {dataType: Types}> | Modelschema
}
export type Dialects = 'mssql' | 'oracle' | 'mysql'  | 'postgres' | 'sqlite';

export interface BuildSchemaOptions {
	mapper?: mapper
	dialect?: Dialects
	sanitize?: boolean
}

export default Modelschema