//////////////////////////////////////////////////////////////////////////////////////////////////
// joiMap requirements
/////////////////////////////////////////////////////////////////////////////////////////////////
import { StringSchema, NumberSchema, DateSchema, BooleanSchema, string, number, date, boolean, array, ArraySchema, any } from 'joi';
import { Types, SchemaItems } from './ModelSchema';

//////////////////////////////////////////////////////////////////////////////////////////////////
/**joiMap
 *
 * parses the value given in the model schema to joi rules for validation.
 *
 * @example
 * joiMap({dataType: String, notNull: true}) => joi.string().required()
 *
 * @param value			: Given SchemaItem object to parse.
 * @param sanitize		: true if strip rule must be applied.
 */
export function joiMap (value: SchemaItems<string, {dataType: Types}> , sanitize?: boolean): StringSchema | NumberSchema | DateSchema | BooleanSchema | ArraySchema {
	let rule: StringSchema | NumberSchema | DateSchema | BooleanSchema | ArraySchema
	switch (value.dataType) {
		case String || 'String':
			rule = string().insensitive()
			rule = addStringRules(rule, value)
			break;
		case Number || 'Number':
			rule = number()
			rule = addNumberRules(rule, value)
			break;
		case 'Integer':
			rule = number().integer()
			rule = addNumberRules(rule, value)
			break;
		case Date || 'Date':
			rule = date()
			rule = addDateRules(rule, value)
			break;
		case Boolean || 'Boolean':
			rule = boolean()
			rule = addBooleanRules(rule, value)
			break;
		case Array || 'Array':
			rule = array()
			rule = addArrayRules(rule, value)
			break;
		default :
			rule = string().insensitive()
			rule = addStringRules(rule, value)
	}
	if (value.defaultValue) {
		rule = rule.default(value.defaultValue)
	}
	if (value.name) {
		rule = rule.label(value.name)
	}
	if (value.hidden && sanitize) {
		rule = rule.strip()
	}
	if (value.notNull && value.notNull === true) {
		rule = rule.required()
	}
	return rule
}

//////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * joiMap Helpers for various data types
 * /


/**addStringRules
 *
 * parses string specific SchemaItems for validation.
 *
 * @example
 * joiMap({dataType: String, notNull: true}) => joi.string().required()
 *
 * @param r			: Given string SchemaItem object to parse.
 */
function addStringRules (r: StringSchema, value: SchemaItems<string, {dataType: Types}>): StringSchema {
	let ru = r as StringSchema
	if (!value.notNull && !value.allowed) {
		ru = ru.allow('')
	}
	if (value.allowed) {
		ru = ru.valid(...value.allowed)
	}
	if (value.trim) {
		ru = ru.trim()
	}
	if (value.max) {
		ru = ru.max(value.max)
	}
	return ru
}
/**addNumberRules
 *
 * parses number specific SchemaItems for validation.
 *
 * @example
 * joiMap({dataType: 'Integer', notNull: true}) => joi.number().integer().required()
 *
 * @param r			: Given number SchemaItem object to parse.
 */
function addNumberRules (r: NumberSchema, value: SchemaItems<string, {dataType: Types}>): NumberSchema {
	let ru = r as NumberSchema
	if (value.allowed) {
		ru = ru.valid(value.allowed)
	}
	if (value.max) {
		ru = ru.max(value.max)
	}
	if (value.min) {
		ru = ru.min(value.min)
	}
	return ru
}
/**addDateRules
 *
 * parses date specific SchemaItems for validation.
 *
 * @example
 * joiMap({dataType: Date, notNull: true}) => joi.date().required()
 *
 * @param r			: Given date SchemaItem object to parse.
 */
function addDateRules (r:  DateSchema, value: SchemaItems<string, {dataType: Types}>):  DateSchema {
	let ru = r as  DateSchema
	if ( value.allowed) {
		ru = ru.valid(value.allowed)
	}
	if (value.max) {
		ru = ru.max(value.max)
	}
	if (value.min) {
		ru = ru.min(value.min)
	}
	return ru
}
/**addBooleanRules
 *
 * parses boolean specific SchemaItems for validation.
 *
 * @example
 * joiMap({dataType: Boolean, notNull: true}) => joi.boolean().required()
 *
 * @param r			: Given boolean SchemaItem object to parse.
 */
function addBooleanRules (r: BooleanSchema, value: SchemaItems<string, {dataType: Types}>): BooleanSchema {
	let ru = r as BooleanSchema
	if ( value.allowed) {
		ru = ru.valid(value.allowed)
	}
	return ru
}
/**addArrayRules
 *
 * parses array specific SchemaItems for validation.
 *
 * @example
 * joiMap({dataType: Array, notNull: true}) => joi.array().required()
 *
 * @param r			: Given array SchemaItem object to parse.
 */
function addArrayRules (r: ArraySchema, value: SchemaItems<string, {dataType: Types}>): ArraySchema {
	let ru = r as ArraySchema
	if (value.contains) {
		switch (value.contains) {
			case String || 'String':
				ru = ru.items(string().insensitive().allow(''))
				break;
			case Number || 'Number':
				ru = ru.items(number())
				break;
			case 'Integer':
				ru = ru.items(number().integer())
				break;
			case Date || 'Date':
				ru = ru.items(date())
				break;
			case Boolean || 'Boolean':
				ru = ru.items(boolean())
				break;
			case Array || 'Array':
				ru = ru.items(array())
				break;
		}
	}
	if (!value.contains) {
		ru = ru.items(any())
	}
	if (value.min) {
		ru = ru.min(value.min)
	}
	if (value.max) {
		ru = ru.max(value.max)
	}
	return ru
}

export default joiMap