//////////////////////////////////////////////////////////////////////////////////////////////////
// SimpleSchema requirements
/////////////////////////////////////////////////////////////////////////////////////////////////
import { object, ObjectSchema, ValidationError, SchemaMap, array, ArraySchema } from 'joi';
import { Modelschema, Dialects, Definition, Technology, SchemaItems, Types } from './ModelSchema';
import { buildSchema } from './buildSchema'
import { forOwn } from 'lodash';

//////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * SimpleSchema interfaces
 */

/**ValidationOutput
 *
 * output of `validate` method with following structure:
 * 		* status	: false if an error was found in validation.
 * 		* content	: original object given in the event user wants to carry on with the error.
 * 		* error		: description of error.
 */
export interface ValidationOutput<Model> {
	/**	false if an error was found in validation. */
	status: boolean
	/**	original object given in the event user wants to carry on with the error. */
	content: Model[]
	/**	description of error. */
	error: ValidationError | null
}

//////////////////////////////////////////////////////////////////////////////////////////////////
/**
 *Simpleschema Class
 */
export class SimpleSchema {

	private _validationSchema: ObjectSchema

	private _sanitizeSchema: ObjectSchema

	private _validationSchemaArray: ArraySchema

	private _sanitizeSchemaArray: ArraySchema

	/** Modelschema given by user accessible from SimpleSchema.schema */
	schema: Modelschema

	/** Current SQL dialect to use for `define` method. */
	dialect: Dialects | undefined

	constructor(schema: Modelschema, dialect?: Dialects) {
		this.dialect = dialect
		this.schema = schema
		this._validationSchema = object().keys(buildSchema(schema))
		this._sanitizeSchema = object().keys(buildSchema(schema, {sanitize: true}))
		this._validationSchemaArray = array()
		this._sanitizeSchemaArray = array()

		this._sanitizeSchemaArray = this._sanitizeSchemaArray.items(this._sanitizeSchema)

		const Arrayrules: {unique: string[]} = arrayRules(this.schema, { unique: [] })
		if (Arrayrules.unique.length > 0) {
			this._validationSchemaArray = this._validationSchemaArray.unique(Arrayrules.unique[0]);
		}

		this._validationSchemaArray = this._validationSchemaArray.items(this._validationSchema)
	}
	/**Schema Validation
	 *
	 * Validate the object against the schema.
	 * Columns that were not defined in the schema will be stripped.
	 *
	 * @example
	 *
	 * let schema = {
	 * 	field : {dataType:String,defaultValue:'default'},
	 * 	field2: {dataType:String,required:true},
	 * }
	 *
	 * const result = SimpleSchema(schema).validate({field:'I am required',field3:'I have not been defined'})
	 * `result => {status : true, content: {field:'I am required',field2:'default'}, error: null}`
	 *
	 * @param obj		: Objec to validate schema against
	 *
	 * @return 			: Promise<{status:boolean, content: object}>
	 */
	validate<Model>(...obj: Model[]): ValidationOutput<Model> {
		const test = this._validationSchemaArray.validate(obj, {
						stripUnknown: {arrays: false, objects: true},
						skipFunctions: true,
						abortEarly: false
					})
		const pass = test.value
		const fail = test.error
		if (fail) {
			return {status: false, content: pass, error: fail}
		} else {
			return {status: true, content: pass, error: fail}
		}
	}
	/**Schema Sanitize
	 *
	 * Works the same as the `validate` method except items with `hidden` rule are stripped from results.
	 *
	 * @example
	 *
	 * let schema = {
	 * 	field : {dataType:String,hidden:true},
	 * 	field2: {dataType:String,required:true},
	 * }
	 *
	 * const result = SimpleSchema(schema).sanitize({field:'I am required',field3:'I have not been defined'})
	 * `result => {status : true, content: {field2:'default'}, error: null}`
	 *
	 * @param obj		: Objec to validate schema against
	 *
	 * @return 			: Promise<{status:boolean, content: object}>
	 */
	sanitize<Model>(...obj: Model[]): ValidationOutput<Model> {
		const test = this._sanitizeSchemaArray.validate(obj, {
						stripUnknown: {arrays: false, objects: true},
						skipFunctions: true,
						abortEarly: false
					})
		const pass = test.value
		const fail = test.error
		if (fail) {
			return {status: false, content: pass, error: fail}
		} else {
			return {status: true, content: pass, error: fail}
		}
	}
	/**Column definition
	 *
	 * Parse schema into a column definition
	 * Currently supports the following technology definitions:
	 * 		* npm-sql
	 *
	 * @example
	 *
	 * let schema = {
	 * 	field : {dataType:String, max:22},
	 * 	field2: {dataType:String, required:true},
	 * }
	 *
	 * const result = await SimpleSchema(schema).define('sql', 'postgres')
	 * `result => {field: {dataType: 'varchar(22)'},field2: {dataType:'varchar(255)'}}`
	 *
	 * @param technology		: technology to parse to. Currently only support sql
	 * @param dialect			: technology to parse to. Currently only support sql
	 *
	 * @return 					: {[x: string]: Definition<string, {dataType: string}>}
	 */
	define(technology: Technology, dialect?: Dialects): SchemaMap | {[x: string]: Definition<string, {dataType: string}>} {
		const d = dialect || this.dialect
		const t = technology
		return buildSchema(this.schema, {mapper: t , dialect: d})
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////
/**
 *Simpleschema Class Helpers
 */

/**ArrayRules
 *
 * loops through schema to identify rules that apply only to arrays.
 * Currently supports:
 * 		* unique
 *
 * @param schema		: Modelschema to parse
 * @param rules			: empty array of columns to populate where rules apply.
 */

function arrayRules (schema: Modelschema, rules: {unique: string[]} ): {unique: string[]} {
	forOwn(schema, (value, key) => {
		if ((value as SchemaItems<string, {dataType: Types}>).dataType) {
			addArrayRules(rules, value as SchemaItems<string, {dataType: Types}>, key);
		} else {
			arrayRules(value as Modelschema, rules)
		}
	})
	return rules
}
/**addArrayRules
 *
 * Applies the rules found in the `ArrayRules` method.
 *
 * @param rules			: empty array of columns to populate where rules apply.
 * @param value			: array SchemaItem to check for array rule.
 */
function addArrayRules (rules: {unique: string[]}, value: SchemaItems<string, {dataType: Types}>, key: string): {unique: string[]} {
	if (value.unique) {
		rules.unique.push(key)
	}
	return rules
}

export default SimpleSchema