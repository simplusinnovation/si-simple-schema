//////////////////////////////////////////////////////////////////////////////////////////////////
// sqlMap requirements
/////////////////////////////////////////////////////////////////////////////////////////////////
import { Definition, Types, SchemaItems, Dialects } from './ModelSchema';

//////////////////////////////////////////////////////////////////////////////////////////////////
/**sqlMap
 *
 * parses the value given in the model schema to sql table definitions.
 *
 * @example
 * sqlMap({dataType: String, notNull: true}) => {varchar(225), NotNull}
 *
 * @param value			: Given SchemaItem object to parse.
 * @param dialect		: dialect to use when parsing to sql.
 */
export function sqlMap (value: SchemaItems<string, {dataType: Types}>, dialect?: Dialects ): Definition<string, {dataType: string}> {
	const technology = dialect ? dialect : 'postgres'
	switch (value.dataType) {
		case String || 'String':
			value.dataType = `VARCHAR(${value.max ? value.max : 255})`
			break;
		case Number || 'Number':
			switch (technology) {
				case 'mssql':
					value.dataType = `NUMERIC(20 , 2)`
					break;
				case 'oracle':
					value.dataType = `NUMBER(* , 2)`
					break;
				case 'mysql':
					value.dataType = `DOUBLE(20 , 2)`
					break;
				case 'postgres':
					value.dataType = `FLOAT(2)`
					break;
				case 'sqlite':
					value.dataType = `DECIMAL(20, 2)`
					break;
			}
			break;
		case 'Integer':
			switch (technology) {
				case 'mssql' || 'postgres' || 'sqlite':
					value.dataType = `BIGINT`
					break;
				case 'oracle':
					value.dataType = `NUMBER(* , 0)`
					break;
				case 'mysql':
					value.dataType = `BIGINT(19)`
					break;
			}
			break;
		case Date || 'Date':
			switch (technology) {
				case 'mssql' || 'mysql' || 'sqlite':
					value.dataType = `DATETIME`
					break;
				case 'oracle' || 'postgres':
					value.dataType = `TIMESTAMP`
					break;
			}
			break;
		case Boolean || 'Boolean':
			value.dataType = `VARCHAR(5)`
			break;
	}
	return value as Definition<string, {dataType: string}>
}

export default sqlMap