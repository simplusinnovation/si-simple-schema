//////////////////////////////////////////////////////////////////////////////////////////////////
// buildSchema requirements
/////////////////////////////////////////////////////////////////////////////////////////////////
import { Modelschema, Types, SchemaItems, BuildSchemaOptions, Definition } from './ModelSchema';
import { joiMap } from './joiMap';
import { sqlMap } from './sqlMap';
import { forOwn } from 'lodash';
import { SchemaMap } from 'joi';

//////////////////////////////////////////////////////////////////////////////////////////////////
/**buildSchema
 *
 * parses object given into sql define or joi validation
 *
 * @param obj				: object to parse
 * @param options			: optional options
 */
export function buildSchema (obj: Modelschema, options?: BuildSchemaOptions): SchemaMap | {[x: string]: Definition<string, {dataType: string}>} {
	const schema: SchemaMap | {[x: string]: Definition<string, {dataType: string}>} = {}
	const map = options ? (options.mapper ? options.mapper : 'joi') : 'joi'
	const dialect = options ? options.dialect : undefined
	const sanitize = options ? options.sanitize : false

	if (map === 'joi') {
		forOwn(obj, (value: SchemaItems<string, {dataType: Types}> | Modelschema, field: string): void => {
			schema[field] = ((value as SchemaItems<string, {dataType: Types}>).dataType) ? joiMap(value as SchemaItems<string, {dataType: Types}>, sanitize) : buildSchema(value as Modelschema, {mapper: 'joi', sanitize: true})
		})
	} else {
		forOwn(obj, (value: SchemaItems<string, {dataType: Types}> | Modelschema, field: string): void => {
			// need to correct null to being a new sql table.
			schema[field] = ((value as SchemaItems<string, {dataType: Types}>).dataType) ? sqlMap(value as SchemaItems<string, {dataType: Types}>, dialect) : null
		})
	}

	return schema
}

export default buildSchema